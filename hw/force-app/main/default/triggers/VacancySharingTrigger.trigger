trigger VacancySharingTrigger on Vacancy__c (after update) {
	new VacancySharingTriggerHandler().run();
}