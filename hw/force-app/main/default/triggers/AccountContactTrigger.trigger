trigger AccountContactTrigger on AccountContact__c (before insert, after update) {
	new AccountContactTriggerHandler().run();
}