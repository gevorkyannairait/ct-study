@isTest
private class VacancySharingTest {
   // Test for the manualShareRead method
   static testMethod void testManualShareRead(){
      // Select users for the test.
      List<User> users = [SELECT Id FROM User WHERE IsActive = true LIMIT 2];
      Id User1Id = users[0].Id;
      Id User2Id = users[1].Id;
   
      // Create new vacancy.
      Vacancy__c vac = new Vacancy__c();
      vac.Name = 'Test Vacancy';
      vac.OwnerId = user1Id;
      insert vac;
                
      // Insert manual share for user who is not record owner.
      System.assertEquals(VacancySharingTriggerHandler.manualShareRead(vac.Id, user2Id), true);
      
      // Query Vacancy sharing records.
      List<Vacancy__Share> vacShrs = [SELECT Id, UserId, AccessLevel, 
         RowCause FROM Vacancy__Share WHERE ParentId = :vac.Id AND UserId= :user2Id];
      
      // Test for only one manual share on vacancy.
      System.assertEquals(vacShrs.size(), 1, 'Set the object\'s sharing model to Private.');
      
      // Test attributes of manual share.
      System.assertEquals(vacShrs[0].AccessLevel, 'Read');
      System.assertEquals(vacShrs[0].RowCause, 'Manual');
      System.assertEquals(vacShrs[0].UserId, user2Id);
      
      // Test invalid job Id.
      delete vac;   
   
      // Insert manual share for deleted job id. 
      System.assertEquals(VacancySharingTriggerHandler.manualShareRead(vac.Id, user2Id), false);
   }  
}