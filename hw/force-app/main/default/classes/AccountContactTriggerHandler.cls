public with sharing class AccountContactTriggerHandler extends TriggerHandler {
    
    private Map<Id, AccountContact__c> OldMap;
    
    public AccountContactTriggerHandler() {
        this.OldMap = (Map<Id, AccountContact__c>) Trigger.OldMap;
    }

    public override void beforeInsert() {

        Set<ID> ContactIds =  new Set<ID>();
  
        for(AccountContact__c accCon:(List<AccountContact__c>) trigger.new){
            ContactIds.add(accCon.contact__c);
            accCon.isPrimary__c = true;
        }

        List<AccountContact__c> allJunctionOfContacts = [Select ID, contact__c FROM AccountContact__c WHERE contact__c in :ContactIds];

        for(AccountContact__c accCon:(List<AccountContact__c>) trigger.new){
            for(AccountContact__c jun:allJunctionOfContacts){
                if(accCon.contact__c==jun.contact__c){
                    accCon.isPrimary__c = false;
                }
            }
        }
    }

    public override void afterUpdate() {

        Set<ID> ContactIds = new Set<ID>();

        for(AccountContact__c accCon:(List<AccountContact__c>) trigger.new){
            AccountContact__c oldAccCon = oldMap.get(accCon.id);
            if(accCon.isPrimary__c != oldAccCon.isPrimary__c){
                ContactIds.add(accCon.contact__c);
            }
        }

        List<AccountContact__c> allJunctionOfContacts = [SELECT CreatedDate, contact__c, isPrimary__c FROM AccountContact__c WHERE contact__c in :ContactIds];

        Map<id,AccountContact__c> mapOfLastJunction = new Map<id,AccountContact__c>();

        for(AccountContact__c accCon:(List<AccountContact__c>) trigger.new){
           for(AccountContact__c jun:allJunctionOfContacts){
               if(jun.id!=accCon.id){
                   if(mapOfLastJunction.get(jun.contact__c) == null){
                       mapOfLastJunction.put(jun.contact__c,jun);
                   }
                   else{
                       if(mapOfLastJunction.get(jun.contact__c).createDate < jun.CreatedDate){
                           mapOfLastJunction.put(jun.contact__c,jun);
                       }
                   }
               }
           } 
        }

        List<AccountContact__c> forUpdate = new List<AccountContact__c>();

        for(AccountContact__c accCon: mapOfLastJunction.values()){
            boolean newValueOfIsPrimary = !accCon.isPrimary__c;
            accCon.isPrimary__c = newValueOfIsPrimary;
            forUpdate.add(accCon);
        }

        TriggerHandler.bypass('AccountContactTriggerHandler');
        update forUpdate;
        TriggerHandler.clearBypass('AccountContactTriggerHandler');

    }
}