public with sharing class VacancySharingTriggerHandler extends VacancySharingTest {

   public override void afterUpdate() {
      
      if(trigger.isUpdate){
         // Create a new list of sharing objects for Vacancy
         List<Vacancy__Share> vacShrs  = new List<Vacancy__Share>();
         
         // Declare variables for user sharing
         Vacancy__Share userShr;
         
         for(Vacancy__c vac : trigger.new){
             // Instantiate the sharing objects
             userShr = new Vacancy__Share();
             
             // Set the ID of record being shared
             userShr.ParentId = vac.Id;
             
             // Set the ID of user being granted access
             userShr.UserId = Vac.Manager__r.User__r.Id;
             
             // Set the access level
             userShr.AccessLevel = 'read';
             
             // Set the Apex sharing reason for hiring manager and recruiter
             userShr.RowCause = Schema.Vacancy__Share.RowCause.User__c;
             
             // Add objects to list for insert
             vacShrs.add(userShr);
         }
         
         // Insert sharing records and capture save result 
         // The false parameter allows for partial processing if multiple records are passed 
         // into the operation 
         Database.SaveResult[] lsr = Database.insert(vacShrs,false);
         
         // Create counter
         Integer i=0;
         
         // Process the save results
         for(Database.SaveResult sr : lsr){
             if(!sr.isSuccess()){
                 // Get the first save result error
                 Database.Error err = sr.getErrors()[0];
                 
                 // Check if the error is related to a trivial access level
                 // Access levels equal or more permissive than the object's default 
                 // access level are not allowed. 
                 // These sharing records are not required and thus an insert exception is 
                 // acceptable. 
                 if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                                &&  err.getMessage().contains('AccessLevel'))){
                     // Throw an error when the error is not related to trivial access level.
                     trigger.newMap.get(vacShrs[i].ParentId).
                       addError(
                        'Unable to grant sharing access due to following exception: '
                        + err.getMessage());
                 }
             }
             i++;
         }   
     }
     
   
   }

}
