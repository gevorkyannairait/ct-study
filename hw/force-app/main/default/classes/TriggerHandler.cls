public virtual class TriggerHandler {
   
    private static Map<string, LoopCount> loopCountMap;
    private static Set<string> bypassHandlers;

    @TestVisible
    private TriggerContext context;

    @TestVisible
    private Boolean isTriggerExecuting;

    static {
        loopCountMap = new Map<String, LoopCount>();
        bypassHandlers = new Set<String>();
    }

    public TriggerHandler() {
        this.setTriggerContext();
    }

    public void run() {
        if(!validateRun()) {
            return;
        }
        
        addToLoopCount();

        switch on this.context {

            when Before_Insert {
                this.beforeInsert();
            }
            when Before_Update {
                this.beforeUpdate();                
            }
            when Before_Delete {
                this.beforeDelete();
            }
            when After_Insert {
                this.afterInsert();
            }
            when After_Update {
                this.afterUpdate();                
            }
            when After_Delete {
                this.afterDelete();
            }
            when After_Undelete {
                this.afterUndelete();
            }
        }
    }

    public void setMaxLoopCount(Integer max) {
        String handlerName = getHandlerName();
        if(!TriggerHandler.loopCountMap.containsKey(handlerName)) {
            TriggerHandler.loopCountMap.put(handlerName, new LoopCount(max));
        } 
        else {
            TriggerHandler.loopCountMap.get(handlerName).setMax(max);
        }
    }

    public void clearMaxLoopCount() {
        this.setMaxLoopCount(-1);
    }

    public static void bypass(String handlerName) {
        TriggerHandler.bypassedHandlers.add(handlerName);
    }

    public static void clearBypass(String handlerName) {
        TriggerHandler.bypassedHandlers.remove(handlerName);
    }

    public static Boolean isBypassed(String handlerName) {
        return TriggerHandler.bypassedHandlers.contains(handlerName);
    }

    public static void clearAllBypasses() {
        TriggerHandler.bypassedHandlers.clear();
    }

    @TestVisible
    private void setTriggerContext() {
        this.setTriggerContext(null, false);
    }

    @TestVisible
    private void setTriggerContext(String ctx, Boolean testMode) {
        if(!Trigger.isExecuting && !testMode) {
            this.isTriggerExecuting = false;
            return;  
        }
        else {
            this.isTriggerExecuting = true;
        }

        if((Trigger.isExecuting && Trigger.isBefore && Trigger.isInsert) ||
             (ctx != null && ctx == 'before insert')) {
            this.context = TriggerContext.Before_Insert;
        }
        else if((Trigger.isExecuting && Trigger.isBefore && Trigger.isUpdate) || 
             (ctx !=null && ctx == 'before update')){
            this.context = TriggerContext.Before_Update;
        }
        else if((Trigger.isExecuting && Trigger.isBefore && Trigger.isDelete) || 
             (ctx !=null && ctx == 'before delete')){
           this.context = TriggerContext.Before_Delete;
        }
        else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isInsert) ||
            (ctx != null && ctx == 'after insert')) {
           this.context = TriggerContext.After_Insert;
        }
       else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isUpdate) || 
            (ctx !=null && ctx == 'after update')){
           this.context = TriggerContext.After_Update;
        }
       else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isDelete) || 
            (ctx !=null && ctx == 'after delete')){
          this.context = TriggerContext.After_Delete;
        }
        else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isUndelete) || 
           (ctx !=null && ctx == 'after undelete')){
         this.context = TriggerContext.After_Undelete;
        }    
    }

    @TestVisible
    private void addToLoopCount() {
        String handlerName = getHandlerName();
        if(TriggerHandler.loopCountMap.containsKey(handlerName)) {
            Boolean exceeded = TriggerHandler.loopCountMap.get(handlerName).increment();
            if(exceeded) {
                Integer max = TriggerHandler.loopCountMap.get(handlerName).max;
                throw new TriggerHandlerException('Maximum loop count of ' + String.valueOf(max) + ' reached in ' + handlerName);
            }
        }
    }

    @TestVisible
    private Boolean validateRun() {
        if(!this.isTriggerExecuting || this.context == null) {
            throw new TriggerHandlerException('Trigger handler called outside of Trigger execution');
        }
        return !TriggerHandler.bypassedHandlers.contains(getHandlerName());
    }

    @TestVisible
    private String getHandlerName() {
        return String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
    }

    @TestVisible
    protected vitrual void beforeInsert(){}
    @TestVisible
    protected vitrual void beforeUpdate(){}
    @TestVisible
    protected vitrual void beforeDelete(){}
    @TestVisible
    protected vitrual void afterInsert(){}
    @TestVisible
    protected vitrual void afterUpdate(){}
    @TestVisible
    protected vitrual void afterDelete(){}
    @TestVisible
    protected vitrual void afterUndelete(){}

    @TestVisible
    private class LoopCount {
        private Integer max;
        private Integer count;

        public LoopCount() {
            this.max = 5;
            this.count = 0;
        }

        public LoopCount(Integer max) {
            this.max = max;
            this.count = 0;
        }

        public Boolean increment() {
            this.count++;
            return this.exceeded();
        }

        public Boolean exceeded() {
            return this.max >= 0 && this.count > this.max;
        }

        public Integer getMax() {
            return this.max;
        }

        public Integer getCount() {
            return this.count;
        }

        public void setMax(Integer max) {
            this.max = max;
        }
    }

    @TestVisible
    private enum TriggerContext {
        Before_Insert, Before_Update, Before_Delete,
        After_Insert, After_Update, After_Delete, After_Undelete
    }

    public class TriggerHandlerException extends Exception {}
}
